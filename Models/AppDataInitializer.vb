﻿Imports System.Collections.Generic
Imports System.Data.Entity



Namespace Models

    Public Class AppDataInitializer
        Inherits DropCreateDatabaseIfModelChanges(Of AppContext)

        '-----------------------------------------------------------------------
        ' TODO デモ用にイニシャライザの基底クラスを変更する
        'CreateDatabaseIfNotExists<T>	データベースが存在しない場合のみ生成
        'DropCreateDatabaseIfModelChanges<T>	モデルが変更された場合のみ再生成
        'DropCreateDatabaseAlways<T>	アプリケーション起動時に常にデータベースを再生成
        '-----------------------------------------------------------------------


        Protected Overrides Sub Seed(context As AppContext)
            'MyBase.Seed(context)

            Console.Write("")

            initTest1(context)



        End Sub

        Private Sub initTest1(context As AppContext)

            Dim a = New Test1()

            a.DataKey = "hoge"
            a.DataValue = "ttt"
            a.AddField = "aaaaaaaa"

            context.Test1s.Add(a)

            context.SaveChanges()

        End Sub





    End Class


End Namespace


