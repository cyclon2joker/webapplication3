﻿
Imports System.Collections.Generic
Imports System.ComponentModel.DataAnnotations


Namespace Models

    Public Class Test1

        Public Property ID As Int32

        <Required, Display(Name:="データキー"), StringLength(20)>
        Public Property DataKey As String

        <Required, Display(Name:="データキー"), StringLength(100)>
        Public Property DataValue As String

        <Display(Name:="Migration test f"), StringLength(100)>
        Public Property AddField As String



        Public Function hoge() As String

            If String.IsNullOrEmpty(DataValue) Then

                Return "[empty value]"
            Else
                Return "[" + DataValue + "]"
            End If


            '            Return

        End Function



    End Class



End Namespace


