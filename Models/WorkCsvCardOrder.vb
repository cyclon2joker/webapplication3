﻿Namespace Models

    Public Class WorkCsvCardOrder

        Public Property ID As Int32

        Public Property UserName As String


        '発注番号
        Public Property OrderNo As String


        'カード番号
        Public Property CardNo As String

        '分番
        Public Property SeqNo As String

        '材料番号
        Public Property ZairyoNo As String

        '数量
        Public Property Suryo As Int32

        Public Property IsChecked As Boolean


        Public Property IsEntried As Boolean

        '発注番号,カード番号,分番,材料番号,数量


        Public Function CheckValue() As String
            If IsChecked Then
                Return "checked"
            End If
            Return ""

        End Function


    End Class


End Namespace

