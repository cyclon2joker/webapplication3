﻿
Imports System
Imports System.Security.Claims
Imports System.Threading.Tasks
Imports System.Web
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin.Security
Imports WebApplication3.Models




Namespace Models


    Public Class HogeUser
        Inherits IdentityUser

        Public Function GenerateUserIdentity(manager As ApplicationUserManager) As ClaimsIdentity
            ' Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            Dim userIdentity = manager.CreateIdentity(Me, DefaultAuthenticationTypes.ApplicationCookie)
            ' Add custom user claims here
            Return userIdentity

        End Function

        Public Function GenerateUserIdentityAsync(manager As ApplicationUserManager) As Task(Of ClaimsIdentity)
            Return Task.FromResult(GenerateUserIdentity(manager))
        End Function

    End Class


    Public Class ApplicationDbContext
        Inherits IdentityDbContext(Of HogeUser)

        Public Sub New()

            MyBase.New("DefaultConnection", throwIfV1Schema:=False)

        End Sub

        Friend Function Create() As ApplicationDbContext


            Return New ApplicationDbContext()

        End Function



    End Class




    '    Public Class IdentityModel

    'End Class






End Namespace

