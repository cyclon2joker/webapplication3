﻿Imports System.Data.Entity

Namespace Models

    Public Class AppContext
        Inherits DbContext

        Sub New()
            MyBase.New("AppConnection")

            Database.SetInitializer(New AppDataInitializer())

        End Sub


        Public Property Test1s As DbSet(Of Test1)

        Public Property WorkCsvCardOrders As DbSet(Of WorkCsvCardOrder)

        Public Property CardOrders As DbSet(Of CardOrder)



    End Class


End Namespace


