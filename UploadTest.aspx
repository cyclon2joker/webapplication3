﻿<%@ Page Title="" Language="vb" AutoEventWireup="false" MasterPageFile="~/Site.Master" CodeBehind="UploadTest.aspx.vb" Inherits="WebApplication3.UploadTest" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <div style="height:40px;"><br/></div>


        <asp:GridView ID="CsvDataList" runat="server" AutoGenerateColumns="False" ShowFooter="False" GridLines="Vertical" CellPadding="4"
        ItemType=" WebApplication3.Models.WorkCsvCardOrder" SelectMethod="GetCsvRows" 
        CssClass="table table-striped table-bordered" >   
        <Columns>

        <asp:TemplateField   HeaderText=" ">            
                <ItemTemplate>
                    <input class="input-form" type="checkbox" name="selected_id_<%#: Item.ID %>" 
                        <%# Item.CheckValue()  %> /> 
                </ItemTemplate>        
        </asp:TemplateField>    

        <asp:BoundField DataField="OrderNo" HeaderText="発注番号" />        
        <asp:BoundField DataField="CardNo" HeaderText="カード番号" />        
        <asp:BoundField DataField="SeqNo" HeaderText="分番" />        
        <asp:BoundField DataField="ZairyoNo" HeaderText="材料番号" />        
        <asp:BoundField DataField="Suryo" HeaderText="数量" />        
        </Columns>    
    </asp:GridView>


<!--
    <table class="table">
    <thead>
      <tr>
        <th>カラム名①</th><th>カラム名②</th><th>カラム名③</th>
      </tr>
    </thead>
    <tbody>
      <tr>
        <td>データ①</td><td>データ②</td><td>データ③</td>
      </tr>
      <tr>
        <td>データ④</td><td>データ⑤</td><td>データ⑥</td>
      </tr>
    </tbody>
    <tfoot>
      <td>補足①</td><td>補足②</td><td>補足③</td>
    </tfoot>
  </table>
-->
    <div style="height:40px;">
        <br/>

    </div>

        
    <div class="text-right">
            <div class="row">
            <asp:FileUpload ID="fileCardOrderCsv" runat="server" />
            </div>
            <div class="row">
            <asp:Button ID="btnUploadTest" runat="server" Text="Upload Test" />
            </div>
    </div>

    <div class="row" style="height:40px;">
        <br/>

    </div>

    <div class="text-right">
            <div class="row">
                <asp:Button ID="btnDoIssue" CssClass="btn btn-primary" runat="server" Text="発行テスト" />
            </div>
    </div>


    <div>
        <asp:PlaceHolder runat="server" ID="errorPanel" ViewStateMode="Disabled" Visible="false">
            <p class="text-danger">
                An error has occurred.
            </p>
        </asp:PlaceHolder>
    </div>




</asp:Content>
