﻿Imports System.Threading.Tasks
Imports Microsoft.AspNet.Identity
Imports Microsoft.AspNet.Identity.EntityFramework
Imports Microsoft.AspNet.Identity.Owin
Imports Microsoft.Owin
Imports Microsoft.Owin.Security
Imports WebApplication3.Models

Public Class ApplicationUserManager
    Inherits UserManager(Of HogeUser)


    Public Sub New(Store As IUserStore(Of HogeUser))

        MyBase.New(Store)

    End Sub


    Friend Function Create(options As IdentityFactoryOptions(Of ApplicationUserManager), context As IOwinContext) As ApplicationUserManager

        Dim manager = New ApplicationUserManager(New UserStore(Of HogeUser)(context.Get(Of ApplicationDbContext)()))
        'Configure validation logic for usernames
        manager.UserValidator = New UserValidator(Of HogeUser)(manager) With
            {
                .AllowOnlyAlphanumericUserNames = False,
                .RequireUniqueEmail = False
            }

        ' Configure validation logic for passwords
        manager.PasswordValidator = New PasswordValidator With
        {
                .RequiredLength = 6,
                .RequireDigit = True
        }



        ' Configure user lockout defaults
        manager.UserLockoutEnabledByDefault = True
        manager.DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5)
        manager.MaxFailedAccessAttemptsBeforeLockout = 5

        Dim dataProtectionProvider = options.DataProtectionProvider
        If Not dataProtectionProvider Is Nothing Then
            manager.UserTokenProvider = New DataProtectorTokenProvider(Of HogeUser)(dataProtectionProvider.Create("ASP.NET Identity"))
        End If
        Return manager

    End Function

End Class


Public Class ApplicationSignInManager
    Inherits SignInManager(Of HogeUser, String)


    Public Sub New(userManager As ApplicationUserManager, authenticationManager As IAuthenticationManager)

        MyBase.New(userManager, authenticationManager)

    End Sub


    ' TODO とりあえず、非同期系は放置。。。
    '    Public Overrides Function CreateUserIdentityAsync(user As HogeUser) As Task(Of HogeUser)
    '    Public Overrides Function CreateUserIdentityAsync(user As HogeUser) As Task(Of Claims)

    'Dim task = user.GenerateUserIdentityAsync(CType(UserManager, ApplicationUserManager))
    'Return task

    'End Function


    '    Public Static ApplicationSignInManager Create(IdentityFactoryOptions<ApplicationSignInManager> options, IOwinContext context)
    '        {
    '   Return New ApplicationSignInManager(context.GetUserManager < ApplicationUserManager > (), context.Authentication);
    '      }


End Class



