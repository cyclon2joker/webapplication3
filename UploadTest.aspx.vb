﻿Imports Microsoft.VisualBasic.FileIO
Imports log4net
Imports System.Data
Imports WebApplication3.Models


Public Class UploadTest
    Inherits System.Web.UI.Page


    Private logger As ILog = LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType)


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

    End Sub

    Private Sub btnUploadTest_Click(sender As Object, e As EventArgs) Handles btnUploadTest.Click

        If fileCardOrderCsv Is Nothing Then

            logger.Debug("////////////////")
            logger.Debug("nothing ....")
            logger.Debug("////////////////")

            Return
        Else

            If fileCardOrderCsv.PostedFile.ContentLength = 0 Then

                logger.Debug("////////////////")
                logger.Debug("nodata ....")
                logger.Debug("////////////////")

                Return

            End If

        End If

        logger.Debug("@////////////////")
        logger.Debug("@data exists....")
        logger.Debug("@////////////////")




        Try
            Dim ctx = New AppContext()

            ctx.Database.ExecuteSqlCommand("delete from WorkCsvCardOrders where username='user1'")
            ctx.SaveChanges()



            Using parser As New TextFieldParser(fileCardOrderCsv.PostedFile.InputStream, Encoding.GetEncoding("shift_jis"))

                parser.TextFieldType = FieldType.Delimited
                parser.SetDelimiters(",")


                Dim lineNo As Int32 = 0

                While Not parser.EndOfData
                    '1行読み込み 
                    Dim row As String() = parser.ReadFields()
                    logger.Debug(row)

                    ' ヘッダをスキップ
                    If lineNo > 0 And row.Count = 5 Then


                        Dim csvRow = New WorkCsvCardOrder() With {
                           .UserName = "user1",
                           .OrderNo = row(0),
                            .CardNo = row(1),
                            .SeqNo = row(2),
                            .ZairyoNo = row(3),
                            .Suryo = Convert.ToInt32(row(4)),
                            .IsChecked = False,
                            .IsEntried = False
                        }

                        Dim q = ctx.CardOrders.Where(Function(c) c.OrderNo = csvRow.OrderNo And
                                                 c.CardNo = csvRow.CardNo And
                                                 c.SeqNo = csvRow.SeqNo)

                        If q.Count < 1 Then
                            ctx.WorkCsvCardOrders.Add(csvRow)
                            ctx.SaveChanges()
                        End If

                        '発注番号, カード番号, 分番, 材料番号, 数量
                        '                        csvRow

                    End If

                    Dim i = 0
                    '                    'DataRow作成 
                    '                    For Each col As String In row
                    '                   logger.Debug(col)
                    '                   i += 1
                    '                  Next
                    lineNo += 1

                End While




            End Using



            CsvDataList.DataBind()






        Catch ex As Exception

            logger.Error("error:" + ex.StackTrace)

        End Try







        'If ((File1.PostedFile!= null) && (File1.PostedFile.ContentLength > 0)) Then{String fn = System.IO.Path.GetFileName(File1.PostedFile.FileName);String SaveLocation = Server.MapPath("Data") + "\\" +  fn;Try{File1.PostedFile.SaveAs(SaveLocation);Response.Write("The file has been uploaded.");}Catch ( Exception ex ){Response.Write("Error: " + ex.Message);//Note: Exception.Message returns a detailed message that describes the current exception. //For security reasons, we do Not recommend that you return Exception.Message to end users in //production environments. It would be better to put a generic error message. }}else{Response.Write("Please select a file to upload.");}



    End Sub

    Private Sub btnDoIssue_Click(sender As Object, e As EventArgs) Handles btnDoIssue.Click

        Dim ctx = New AppContext()

        ctx.Database.ExecuteSqlCommand(
                "Update WorkCsvCardOrders set IsChecked = 0 where username='user1' and IsEntried = 0")
        ctx.SaveChanges()

        Dim checkedList = Context.Request.Params.AllKeys.Where(Function(k) k.StartsWith("selected_id_")).ToList

        If checkedList.Count = 0 Then

            logger.Debug("no selected rows...")
        Else




            For Each key In checkedList

                logger.Debug("selected:[" + key + "]")

                Dim id As Int32 = Convert.ToInt32(key.Replace("selected_id_", ""))

                Dim csvRow = ctx.WorkCsvCardOrders.Where(Function(w) w.ID = id).First

                ' チェック状態を取り合ず保存
                csvRow.IsChecked = True
                ctx.SaveChanges()


                Dim q = ctx.CardOrders.Where(Function(c) c.OrderNo = csvRow.OrderNo And
                                                 c.CardNo = csvRow.CardNo And
                                                 c.SeqNo = csvRow.SeqNo)

                If q.Count < 1 Then
                    Dim order = New CardOrder() With {
                        .OrderNo = csvRow.OrderNo,
                        .CardNo = csvRow.CardNo,
                        .SeqNo = csvRow.SeqNo,
                        .ZairyoNo = csvRow.ZairyoNo,
                        .Suryo = csvRow.Suryo,
                        .Isissued = True
                    }

                    ctx.CardOrders.Add(order)
                    csvRow.IsEntried = True

                    ctx.SaveChanges()

                End If

            Next


        End If

        CsvDataList.DataBind()



    End Sub

    Public Function GetCsvRows() As List(Of WorkCsvCardOrder)

        Dim ctx = New AppContext()

        Return ctx.WorkCsvCardOrders.Where(Function(w) w.UserName = "user1" And w.IsEntried = False).ToList




    End Function


End Class