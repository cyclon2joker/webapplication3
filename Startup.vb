﻿Imports Microsoft.AspNet.Identity
Imports Microsoft.Owin
Imports Microsoft.Owin.Security.Cookies
Imports Owin


<Assembly: OwinStartup(GetType(WebApplication3.Startup))>


Public Class Startup

    Public Sub Configuration(app As IAppBuilder)


        app.UseCookieAuthentication(New CookieAuthenticationOptions() With
            {.AuthenticationType = DefaultAuthenticationTypes.ApplicationCookie,
             .LoginPath = New PathString("/Login")})

    End Sub



End Class
